// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import router from './router'

require('jquery');
import 'font-awesome/css/font-awesome.css'
import 'font-awesome/fonts/fontawesome-webfont.svg'
import 'materialize-css/dist/js/materialize.min.js'
import 'materialize-css/dist/css/materialize.min.css'



Vue.config.productionTip = false;

/* eslint-disable no-new */
Vue.directive("select", {
    "twoWay": true,

    update: function(el, binding, vnode) {
        if(!vnode.elm.dataset.vueSelectReady) {

            $(el).on('change', function() {
                vnode.context.$set(vnode.context, binding.expression, el.value);
            });

            $(el).material_select();
            vnode.elm.dataset.vueSelectReady = true
        }
    },

    unbind: function(el, binding, vnode) {
        $(el).material_select('destroy');
    }
});

Vue.use(Vuex)

const store = new Vuex.Store({
    state : {
        selectedWorkshops : [],
        nationalCode : ``,
        btiuID : ``
    }
})

new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
})
