import Vue from 'vue'
import Router from 'vue-router'
import NationalCodePage from '@/components/NationalCodePage'
import WorkshopSelection from '@/components/WorkshopSelection'
import Success from '@/components/Success'




Vue.use(Router);

export default new Router({
  routes: [
        {
          path: '/',
          name: 'NationalCodePage',
          component: NationalCodePage
        },
      {
          path: '/workshopSelection',
          name: 'WorkshopSelection',
          component: WorkshopSelection
        },
      {
          path: '/success',
          name: 'Success',
          component: Success
        }
  ]
})
